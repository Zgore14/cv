//
//  SkillsViewController.swift
//  CV
//
//  Created by Pierre Zawadil on 2018-10-31.
//  Copyright © 2018 Pierre Zawadil. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var images: [UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Skills"
        images = [
            UIImage(named: "tile000"),
            UIImage(named: "tile001"),
            UIImage(named: "tile002"),
            UIImage(named: "tile003"),
            UIImage(named: "tile004"),
            UIImage(named: "tile005")
            ] as! [UIImage]
        imageView.animationImages = images
        imageView.animationDuration = 0.5
        imageView.startAnimating()
    }

}
