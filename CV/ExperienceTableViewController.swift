//
//  ExperienceTableViewController.swift
//  CV
//
//  Created by Pierre Zawadil on 31/10/2018.
//  Copyright © 2018 Pierre Zawadil. All rights reserved.
//

import UIKit

struct Experience {
    
    var id : Int
    var date : String
    var title : String
    var text : String
    var image : String
}


struct ExperienceSection {
    
    var title: String
    var experiences: [Experience]
}

class ExperienceTableViewController: UITableViewController {
    
    var data = [
        ExperienceSection(title: "Work Experience", experiences: [
            Experience(id: 1, date: "2015", title: "Storyplayr", text: "Backend / Front development and maintenance of a platform for online children books. Tech: Node.js, AngularJS.", image: "storyplayr-icon"),
            Experience(id: 2, date: "2017-2018", title: "Lab vente-privée.com", text: "IT Innovation lab of the compagny vente-privée.com. Multiple project realised containing a project around robots and photos. Tech: Ruby, Docker, C++.", image: "vente-privee-icon"),
            ]),
        ExperienceSection(title: "Education", experiences: [
            Experience(id: 3, date: "2015 - current", title: "Epitech", text: "Multiple IT project realised, and intensive learning on C and C++.", image: "epitech-icon"),
            ]),
        ]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Experience"
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExperienceDetailViewController {
            if let indexPath = sender as? IndexPath {
                destination.experience = data[indexPath.section].experiences[indexPath.row]
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return data.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return data[section].experiences.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return data[section].title
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)

        let exp = data[indexPath.section].experiences[indexPath.row]
        cell.textLabel?.text = exp.title
        cell.detailTextLabel?.text = exp.date
        cell.imageView?.image = UIImage(named: exp.image)
        let imageSize = CGSize.init(width: 25, height: 25)
        UIGraphicsBeginImageContextWithOptions(imageSize, false, UIScreen.main.scale)
        let imageRect = CGRect.init(origin: CGPoint.zero, size: imageSize)
        cell.imageView?.image!.draw(in: imageRect)
        cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return cell
    }
	
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ExperienceDetailsSegue", sender: indexPath)
    }
    
}
