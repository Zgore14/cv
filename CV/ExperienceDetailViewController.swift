//
//  ExperienceDetailViewController.swift
//  CV
//
//  Created by Pierre Zawadil on 2018-10-31.
//  Copyright © 2018 Pierre Zawadil. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    @IBOutlet weak var expImage: UIImageView!
    @IBOutlet weak var expTitle: UILabel!
    @IBOutlet weak var expDate: UILabel!
    @IBOutlet weak var expText: UITextView!
    
    var experience: Experience = Experience(id: -1, date: "None", title: "None", text: "None", image: "None")
    
    override func viewDidLoad() {
        print("HERE1")
        super.viewDidLoad()
        self.title = experience.title
        print("HERE2")
        expImage.image = UIImage(named: experience.image)
        expTitle.text = experience.title
        expDate.text = experience.date
        expText.text = experience.text
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
